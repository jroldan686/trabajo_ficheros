package acdat_tema2.jrl.acdat.descargadeimagenesyfrases;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

public class TareaAsincrona extends AsyncTask<String, Integer, Resultado> {

    public static final String HTTP = "http";
    public static final String HTTPS = "https";
    public static final String FTP = "ftp";

    private ProgressDialog progreso;
    private Context contexto;
    private MainActivity mainActivity;

    public TareaAsincrona(Context contexto, MainActivity mainActivity) {
        this.contexto = contexto;
        this.mainActivity = mainActivity;
    }
    protected void onPreExecute() {
        progreso = new ProgressDialog(contexto);
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Conectando . . .");
        progreso.setCancelable(true);
        progreso.show();
    }
    protected Resultado doInBackground(String... cadena) {
        Resultado resultado = new Resultado();
        try {
            if (cadena[0].equals(mainActivity.IMAGEN)) {
                resultado.setCodigo(mainActivity.descargarFtp());
            }
            if (cadena[0].equals(mainActivity.TEXTO)) {
                resultado.setCodigo(mainActivity.descargarFtp());
            }
            if (cadena[0].equals(mainActivity.ERROR)) {
                resultado.setCodigo(mainActivity.cargarErroresAFtp());
            } else {
                resultado = null;
                cancel(true);
            }
        } catch (Exception e) {
            if(!mainActivity.registrarError(mainActivity.NOMBRE_FICHERO_ERRORES, e))
                Toast.makeText(contexto, "No ha sido posible registrar el error: " + e.getMessage() + " en el fichero " + mainActivity.ficheroErrores.getPath(), Toast.LENGTH_LONG).show();
            else
                Toast.makeText(contexto, e.getMessage(), Toast.LENGTH_SHORT).show();
            resultado = null;
            cancel(true);
        }
        return resultado;
    }
    protected void onPostExecute(Resultado resultado) {
        progreso.dismiss();
    }
    protected void onCancelled() {
        progreso.dismiss();
    }
}
