package acdat_tema2.jrl.acdat.descargadeimagenesyfrases;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import cz.msebera.android.httpclient.entity.mime.Header;

public class MainActivity extends Activity implements View.OnClickListener {

    static final String NOMBRE_FICHERO_INTERVALO = "intervalo.txt";
    static final String NOMBRE_FICHERO_CONFIG = "config.txt";
    static final String NOMBRE_FICHERO_ERRORES = "errores.txt";
    public static final String IMAGEN = "imagen";
    public static final String TEXTO = "texto";
    public static final String ERROR = "error";

    String host, user, password, protocolo, directorio, fichero;
    int port, intervalo;
    File ficheroIntervalo, ficheroConfig, ficheroErrores;

    RadioButton rbnHttpImagenes, rbnFtpImagenes, rbnHttpFrases,rbnFtpFrases;
    EditText edtImagenes, edtFrases;
    Button btnDescargar;
    ImageView imgvImagen;
    TextView txvFrase;

    Memoria memoria;
    Resultado resultado;
    FTPConnection conexionFtp;
    TareaAsincrona tareaAsincronaImagenes;
    TareaAsincrona tareaAsincronaFrases;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rbnHttpImagenes = (RadioButton)findViewById(R.id.rbnHttpImagenes);
        rbnFtpImagenes = (RadioButton)findViewById(R.id.rbnFtpImagenes);
        rbnHttpFrases = (RadioButton)findViewById(R.id.rbnHttpFrases);
        rbnFtpFrases = (RadioButton)findViewById(R.id.rbnFtpFrases);
        edtImagenes = (EditText)findViewById(R.id.edtImagenes);
        edtFrases = (EditText)findViewById(R.id.edtFrases);
        btnDescargar = (Button)findViewById(R.id.btnDescargar);
        btnDescargar.setOnClickListener(this);
        imgvImagen = (ImageView)findViewById(R.id.imgvImagen);
        txvFrase = (TextView)findViewById(R.id.txvFrase);
        txvFrase.setText("");

        memoria = new Memoria(this);
        resultado = new Resultado();
        conexionFtp = new FTPConnection();
        ficheroIntervalo = new File(NOMBRE_FICHERO_INTERVALO);
        ficheroConfig = new File(NOMBRE_FICHERO_CONFIG);
        ficheroErrores = new File(NOMBRE_FICHERO_ERRORES);

        // La siguiente línea sólo se decomentará cuando no se utilice conexión a un servidor FTP de manera asíncrona
        //StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitNetwork().build());
    }

    @Override
    public void onClick(View v) {
        if(v == btnDescargar) {
            leerConfiguracion();
            leerIntervalo();
            if(rbnHttpImagenes.isChecked()) {
                if(obtenerDatosServidor(edtImagenes.getText().toString(), false)) {
                    //if (this.protocolo == "http" || this.protocolo == "https")
                        //descargarHttp();
                    //TareaAsincrona tareaAsincrona = new TareaAsincrona(this, MainActivity.this);
                    //tareaAsincrona.execute(this.ERROR, this.protocolo, this.fichero);
                }
            }
            if(rbnFtpImagenes.isChecked()) {
                if(obtenerDatosServidor(edtImagenes.getText().toString(), true)) {
                    if (this.protocolo == "ftp") {
                        tareaAsincronaImagenes = new TareaAsincrona(this, MainActivity.this);
                        tareaAsincronaImagenes.execute(this.IMAGEN, this.protocolo, this.fichero);
                        String[] lineasFichero = leerFichero(this.fichero);
                        for (int i = 0; i < lineasFichero.length; i++) {
                            try {
                                URL rutaImagen = new URL(lineasFichero[i]);
                                Picasso.with(MainActivity.this).load(rutaImagen.toString()).into(imgvImagen);
                            } catch (Exception e) {
                                if (!registrarError(NOMBRE_FICHERO_ERRORES, e))
                                    Toast.makeText(this, "No ha sido posible registrar el error: " + e.getMessage() + " en el fichero " + ficheroErrores.getPath(), Toast.LENGTH_LONG).show();
                                else
                                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        TareaAsincrona tareaAsincrona = new TareaAsincrona(this, MainActivity.this);
                        tareaAsincrona.execute(this.ERROR, this.protocolo, this.fichero);
                    }
                }
            }
            if(rbnHttpFrases.isChecked()) {
                if(obtenerDatosServidor(edtFrases.getText().toString(), false)) {
                    //if (this.protocolo == "http" || this.protocolo == "https")
                        //descargarHttp();
                    //TareaAsincrona tareaAsincrona = new TareaAsincrona(this, MainActivity.this);
                    //tareaAsincrona.execute(this.ERROR, this.protocolo, this.fichero);
                }
            }
            if(rbnFtpFrases.isChecked()) {
                if(obtenerDatosServidor(edtFrases.getText().toString(), true)) {
                    if (this.protocolo == "ftp") {
                        tareaAsincronaFrases = new TareaAsincrona(this, MainActivity.this);
                        tareaAsincronaFrases.execute(this.TEXTO, this.protocolo, this.fichero);
                        String[] lineasFichero = leerFichero(this.fichero);
                        for (int i = 0; i < lineasFichero.length; i++) {
                            try {
                                txvFrase.setText(lineasFichero[i]);
                            } catch (Exception e) {
                                if (!registrarError(NOMBRE_FICHERO_ERRORES, e))
                                    Toast.makeText(this, "No ha sido posible registrar el error: " + e.getMessage() + " en el fichero " + ficheroErrores.getPath(), Toast.LENGTH_LONG).show();
                                else
                                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    TareaAsincrona tareaAsincrona = new TareaAsincrona(this, MainActivity.this);
                    tareaAsincrona.execute(this.ERROR, this.protocolo, this.fichero);
                }
            }
        }
    }

    protected synchronized boolean obtenerDatosServidor(String ruta, boolean esFTP) {
        if(ruta.isEmpty())
            return false;
        if(esFTP) {
            this.port = 21;
            this.protocolo = "ftp";
        } else {
            this.port = 80;
            this.protocolo = "http";
        }
        try {
            if(ruta.contains("https://") && !esFTP)
                this.protocolo += "s";
            // Si la ruta no contiene ya uno de estos protocolos conocidos, se le añadira el protocolo correspondiente
            if(!ruta.contains("http://") && !ruta.contains("https://") && !ruta.contains("ftp://"))
                ruta = this.protocolo + "://" + ruta;
            URL enlace = new URL(ruta);
            // Si la URL ya contiene un puerto y no es igual a su puerto correspondiente, el puerto de la URL no sera valido
            if(enlace.getPort() != -1 && enlace.getPort() != this.port)
                throw new Exception("El puerto de la URL no es valido");
            // Si el protocolo de la URL no es igual al protocolo seleccionado, o bien,
            // el protocolo de la URL es igual a https siendo la URL para ftp, el protocolo de la URL no sera valido
            if((!enlace.getProtocol().equals(this.protocolo)) || (enlace.getProtocol().equals("https") && esFTP))
                throw new Exception("El protocolo de la URL no es valido");
            this.host = enlace.getHost();
            this.directorio = ".";      // El punto representa a la ruta relativa
            String[] carpetas = enlace.getFile().split("/");
            for(int i = 0; i < carpetas.length - 1; i++) {
                if(i == carpetas.length - 2)
                    this.directorio += carpetas[i];
                else
                    this.directorio += carpetas[i] + "/";
            }
            this.fichero = carpetas[carpetas.length - 1];
            return true;
        } catch (Exception e) {
            if(!registrarError(NOMBRE_FICHERO_ERRORES, e))
                Toast.makeText(this, "No ha sido posible registrar el error: " + e.getMessage() + " en el fichero " + ficheroErrores.getPath(), Toast.LENGTH_LONG).show();
            else
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    /*
    protected void cambiarImagen(String rutaImagen) {
        //MyCountDownTimer temporizador = new MyCountDownTimer(1000, intervalo);
    }
    */

    protected synchronized boolean registrarError(String rutaFichero, Exception excepcion) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String fechaHoraActual = sdf.format(new Date());
        String error = rutaFichero + ":" + fechaHoraActual + ":" + excepcion.getMessage() + ";\n";
        // Si la memoria externa no está disponible para escritura, se intentará escribir en la memoria interna
        if(memoria.disponibleEscritura()) {
            if(!memoria.escribirExterna(NOMBRE_FICHERO_ERRORES, error, true, "UTF-8")) {
                Toast.makeText(this, "Error de escritura en el fichero de errores de la memoria externa",Toast.LENGTH_SHORT).show();
                return false;
            }
        } else if(!memoria.escribirInterna(NOMBRE_FICHERO_ERRORES, error, true, "UTF-8")) {
            Toast.makeText(this, "Error de escritura en el fichero de errores de la memoria interna",Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    protected boolean leerConfiguracion() {
        if((resultado = memoria.leerAsset(NOMBRE_FICHERO_CONFIG)).getCodigo()) {
            try {
                String[] datosConfig = resultado.getContenido().split("\n");
                this.user = datosConfig[0];
                this.password = datosConfig[1];
                return true;
            } catch (Exception e) {
                Toast.makeText(this, "Error en la conversion de los datos de configuracion",Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Error de lectura del fichero de configuracion",Toast.LENGTH_SHORT).show();
        }
        this.user = "";
        this.password = "";
        return false;
    }

    protected boolean leerIntervalo() {
        // Se lee el fichero raw sin la extensión del nombre del archivo
        if((resultado = memoria.leerRaw(NOMBRE_FICHERO_INTERVALO.substring(0, NOMBRE_FICHERO_INTERVALO.length() - 4))).getCodigo()) {
            try {
                this.intervalo = Integer.valueOf(resultado.getContenido());
                return true;
            } catch (Exception e) {
                Toast.makeText(this, "Error en la conversion de los datos de intervalo",Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Error de lectura del fichero de intervalo",Toast.LENGTH_SHORT).show();
        }
        this.intervalo = 0;
        return false;
    }

    protected synchronized String[] leerFichero(String nombreFichero) {
        Resultado resultado;
        if (memoria.disponibleLectura()) {
            if ((resultado = memoria.leerExterna(nombreFichero, "UTF-8")).getCodigo()) {
                return resultado.getContenido().split("\n");
            } else {
                try {
                    throw new Exception("Error al leer el fichero " + this.fichero + " descargado en la memoria externa");
                } catch (Exception e) {
                    if (!registrarError(NOMBRE_FICHERO_ERRORES, e))
                        Toast.makeText(this, "No ha sido posible registrar el error: " + e.getMessage() + " en el fichero " + ficheroErrores.getPath(), Toast.LENGTH_LONG).show();
                    else
                        Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            try {
                throw new Exception("La memoria externa no esta disponible para la lectura del fichero descargado");
            } catch (Exception e) {
                if (!registrarError(NOMBRE_FICHERO_ERRORES, e))
                    Toast.makeText(this, "No ha sido posible registrar el error: " + e.getMessage() + " en el fichero " + ficheroErrores.getPath(), Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
        return null;
    }

    /*
    protected synchronized String[] descargarHttp() {
        final StringBuilder informacion = new StringBuilder();
        String url = this.protocolo + "://" + this.host + ":" + this.port + this.directorio + "/" + this.fichero;
        AsyncHttpClient cliente = new AsyncHttpClient();
        final ProgressDialog progreso = new ProgressDialog(MainActivity.this);
        cliente.get(url, new FileAsyncHttpResponseHandler(MainActivity.this) {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                try {
                    throw new Exception("Se ha producido un error al acceder al fichero:\n" + throwable.getMessage());
                } catch (Exception e) {
                    if (!registrarError(NOMBRE_FICHERO_ERRORES, e))
                        Toast.makeText(MainActivity.this, "No ha sido posible registrar el error: " + e.getMessage() + " en el fichero " + ficheroErrores.getPath(), Toast.LENGTH_LONG).show();
                    else
                        Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, File response) {
                progreso.dismiss();
                try {
                    InputStream file = openFileInput(response.getAbsolutePath());
                    InputStreamReader contenido = new InputStreamReader(file);
                    BufferedReader br = new BufferedReader(contenido);
                    String linea;
                    while((linea = br.readLine()) != null)
                        informacion.append(linea);
                } catch (Exception e) {
                    if (!registrarError(NOMBRE_FICHERO_ERRORES, e))
                        Toast.makeText(MainActivity.this, "No ha sido posible registrar el error: " + e.getMessage() + " en el fichero " + ficheroErrores.getPath(), Toast.LENGTH_LONG).show();
                    else
                        Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    */

    protected synchronized boolean descargarFtp() {
        File tarjeta, miFichero;
        if (memoria.disponibleEscritura()) {
            tarjeta = Environment.getExternalStorageDirectory();
            miFichero = new File(tarjeta, this.fichero);
            if (conexionFtp.ftpConnect(this.host, this.port, this.user, this.password)) {
                if (conexionFtp.ftpDownload(this.fichero, this.directorio , miFichero)) {
                    conexionFtp.ftpDisconnect();
                    return true;
                } else {
                    try {
                        throw new Exception("Error al descargar el fichero: " + miFichero.getAbsolutePath());
                    } catch (Exception e) {
                        if (!registrarError(NOMBRE_FICHERO_ERRORES, e))
                            Toast.makeText(MainActivity.this, "No ha sido posible registrar el error: " + e.getMessage() + " en el fichero " + ficheroErrores.getPath(), Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
        return false;
    }

    protected boolean cargarErroresAFtp() {
        File tarjeta = Environment.getExternalStorageDirectory();
        File miFichero = new File(tarjeta.getAbsolutePath(), NOMBRE_FICHERO_ERRORES);
        if (memoria.disponibleLectura()) {
            if ((resultado = memoria.leerExterna(NOMBRE_FICHERO_ERRORES, "UTF-8")).getCodigo()) {

                if (conexionFtp.ftpConnect(this.host, this.port, this.user, this.password)) {
                    if (conexionFtp.ftpUpload(miFichero, NOMBRE_FICHERO_ERRORES, this.directorio)) {
                        conexionFtp.ftpDisconnect();
                        return true;
                    } else {
                        Toast.makeText(MainActivity.this, "Error al subir el fichero", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        } else {
            if ((resultado = memoria.leerInterna(NOMBRE_FICHERO_ERRORES, "UTF-8")).getCodigo()) {

                if (conexionFtp.ftpConnect(this.host, this.port, this.user, this.password)) {
                    if (conexionFtp.ftpUpload(miFichero, NOMBRE_FICHERO_ERRORES, this.directorio)) {
                        conexionFtp.ftpDisconnect();
                        return true;
                    } else {
                        Toast.makeText(MainActivity.this, "Error al subir el fichero", Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                Toast.makeText(MainActivity.this, "Error al escribir el fichero en mem. externa", Toast.LENGTH_SHORT).show();
            }
        }
        return false;
    }
}
