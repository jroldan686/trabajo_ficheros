package acdat_tema2.jrl.acdat.descargadeimagenesyfrases;

import android.app.Activity;
import android.os.CountDownTimer;

public class MyCountDownTimer extends CountDownTimer {

    public MyCountDownTimer(long startTime, long interval) {
        super(startTime, interval);
    }

    @Override
    public void onTick(long millisUntilFinished) {

    }

    @Override
    public void onFinish() {

    }
}
